#!/bin/zsh

set -e

git clone https://gitlab.com/t0fik/zsh-custom.git ${HOME}/.zshrc.d

if [[ "$(head -n1 ${HOME}/.zshrc)" = "# t0fik/zsh-custom applied" ]];then
  echo "Already installed"
  exit 0
fi

cp -f ${HOME}/.zshrc{,.pre-custom}
awk 'BEGIN{print "# t0fik/zsh-custom applied"}
/^\s*ZSH_CUSTOM/{next}
{print}
/^# ZSH_CUSTOM/{print "ZSH_CUSTOM=\${HOME}/.zshrc.d"}
' ${HOME}/.zshrc.pre-custom > ${HOME}/.zshrc

exit 0
